package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.*;

public class ExcelNew {

	public static void main(String [] args){
		String line;
		StringBuilder outputText;
		int classSize;
		ArrayList<String> firstNameArrayList = new ArrayList<String>();
		ArrayList<String> surnameArrayList = new ArrayList<String>();
		try{

			readFileToList(firstNameArrayList, "FirstNames.txt");
			readFileToList(surnameArrayList, "Surnames.txt");

			classSize = 550;
			outputText = new StringBuilder();
			for(int i = 1; i <= classSize; i++){
				String student = "";
				student = setID(i, student);

				int randFNIndex = randFNIndexGenerate(firstNameArrayList);
				int randSNIndex = randFNIndexGenerate(surnameArrayList);

				student += "\t" + surnameArrayList.get(randSNIndex) + "\t" + firstNameArrayList.get(randFNIndex) + "\t";
				//Student Skill
				int randStudentSkill = getRandStudentScore();
				//Labs//////////////////////////
				int numLabs = 3;
				for(int j = 0; j < numLabs; j++){
					student = getString(student, randStudentSkill, 5, 15, 25, 65, false);
					student += "\t";
				}
				//Test/////////////////////////
				student = getString(student, randStudentSkill, 5, 20, 65, 90, false);
				student += "\t";
				///////////////Exam////////////
				student = getString(student, randStudentSkill, 7, 20, 60, 90, true);
				//////////////////////////////////
				student += "\n";
				outputText.append(student);
			}
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
			bw.write(outputText.toString());
			bw.close();
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
	}

	private static void readFileToList(ArrayList<String> list, String fileName) throws IOException {
		String line;BufferedReader br = new BufferedReader(new FileReader(fileName));
		while((line = br.readLine())!= null){
            list.add(line);
        }
		br.close();
	}

	private static String getString(String student, int randStudentSkill, int lower, int midlow, int mid, int upper, boolean userSpecialThing) {
		if(randStudentSkill <= lower){
			int randDNSProb = getRandStudentScore();
			if(userSpecialThing && randDNSProb <= 5){
				student += ""; //DNS

			}else{
				student = randomNoGenerate(student);
			}
        }else if((randStudentSkill > lower) && (randStudentSkill <= midlow)){
                student += (getAnInt(10, 40)); //[40,49]
        } else if((randStudentSkill > midlow) && (randStudentSkill <= mid)){
            student += (getAnInt(20, 50)); //[50,69]
        } else if((randStudentSkill > mid) && (randStudentSkill <= upper)){
            student += (getAnInt(20, 70)); //[70,89]
        } else{
            student += (getAnInt(11, 90)); //[90,100]
        }
		return student;
	}

	private static int getAnInt(int a, int b) {
		return (int)(Math.random()* a) + b;
	}

	private static int randFNIndexGenerate(ArrayList arrayName){
		return (int)(Math.random()*arrayName.size());

	}

	private static int getRandStudentScore() {
		return (int)(Math.random()*101);
	}

	private static String randomNoGenerate(String student) {
		student += (int)(Math.random()*40); //[0,39]
		return student;
	}

	private static String setID(int i, String student) {
		if(i/10 < 1){
            student += "000" + i;
        }else if (i/100 < 1){
            student += "00" + i;
        }else if (i/1000 < 1){
            student += "0"+i;
        }else{
            student += i;
        }
		return student;
	}
}
